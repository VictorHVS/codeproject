<?php
/**
 * Created by IntelliJ IDEA.
 * User: Paulo Henrique
 * Date: 12/12/2015
 * Time: 18:49
 */

namespace codeproject\Repositories;


use codeproject\Entities\Client;
use Prettus\Repository\Eloquent\BaseRepository;

class ClientRepositoryEloquent extends BaseRepository implements ClientRepository
{

    public function model()
    {
        return Client::class;
    }
}