<?php
/**
 * Created by IntelliJ IDEA.
 * User: Paulo Henrique
 * Date: 13/12/2015
 * Time: 14:56
 */

namespace codeproject\Repositories;


use Prettus\Repository\Contracts\RepositoryInterface;

interface ClientRepository extends RepositoryInterface
{

}