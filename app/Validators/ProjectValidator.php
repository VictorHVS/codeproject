<?php
/**
 * Created by IntelliJ IDEA.
 * User: Paulo Henrique
 * Date: 18/12/2015
 * Time: 00:19
 */

namespace codeproject\Validators;


use Prettus\Validator\LaravelValidator;

class ProjectValidator extends LaravelValidator
{

    protected $rules = [
        'owner_id' =>'required|integer',
        'client_id' => 'required|integer',
        'name' => 'required',
        'status' => 'required',
        'progress' => 'required|integer|between:1, 100',
        'due_date' => 'required|date'
    ];

}