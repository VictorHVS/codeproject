<?php
/**
 * Created by IntelliJ IDEA.
 * User: Paulo Henrique
 * Date: 18/12/2015
 * Time: 00:19
 */

namespace codeproject\Validators;


use Prettus\Validator\LaravelValidator;

class ClientValidator extends LaravelValidator
{

    protected $rules = [
        'name' =>'required|max:255',
        'responsible' => 'required|max:255',
        'email' => 'required|email',
        'phone' => 'required',
        'address' => 'required'
    ];

}