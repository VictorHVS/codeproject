<?php
/**
 * Created by IntelliJ IDEA.
 * User: Paulo Henrique
 * Date: 16/12/2015
 * Time: 23:49
 */

namespace codeproject\Services;


use codeproject\Repositories\ProjectRepository;
use codeproject\Validators\ProjectValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectService
{
    protected $repository;
    protected $validator;

    public function __construct(ProjectRepository $projectRepository, ProjectValidator $projectValidator)
    {
        $this->repository = $projectRepository;
        $this->validator = $projectValidator;
    }

    public function create(array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function show($id){
        try{
            $this->repository->with(['owner', 'client'])->find($id);
        } catch (\Exception $e){
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function update(array $data, $id)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function delete($id){
        try {
            $this->repository->delete($id);
        } catch(\Exception $e){
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function addMember($id, $idMember){
        try {
            $this->repository->find($id);
        } catch(\Exception $e){
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function removeMember(){
        try {
        } catch(\Exception $e){
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function isMember($id, $idMember){
        try {
//            $isMember = $this->repository->findWhere(['id' => $id, ]);
//
//            if(isset($isMember)){
//
//            }else{
//
//            }
        } catch(\Exception $e){
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }
}