<?php
/**
 * Created by IntelliJ IDEA.
 * User: Paulo Henrique
 * Date: 16/12/2015
 * Time: 23:49
 */

namespace codeproject\Services;


use codeproject\Repositories\ClientRepository;
use codeproject\Validators\ClientValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class ClientService
{
    protected $repository;
    protected $validator;

    public function __construct(ClientRepository $clientRepository, ClientValidator $clientValidator)
    {
        $this->repository = $clientRepository;
        $this->validator = $clientValidator;
    }

    public function show($id)
    {
        try{
            return $this->repository->find($id);
        }catch (\Exception $e){
            return [
            'error' => true,
            'message' => $e->getMessage()
            ];
        }

    }

    public function create(array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }

    }

    public function update(array $data, $id)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        } catch(\Exception $e){
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function delete($id)
    {
        try {
            $projects = $this->repository->find($id)->projects;
            foreach($projects as $project){
                //todo chamar o ProjectService->delete()
                $project->delete();
            }
            $this->repository->delete($id);

        } catch(\Exception $e){
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }
}