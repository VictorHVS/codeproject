<?php

namespace codeproject\Http\Controllers;

use codeproject\Repositories\ProjectRepository;
use codeproject\Services\ProjectService;
use Illuminate\Http\Request;

use codeproject\Http\Requests;

class ProjectController extends Controller
{
    private $repository;
    private $service;

    public function __construct(ProjectRepository $repository, ProjectService $service)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->repository->with(['client','owner'])->all();
    }


    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    public function show($id)
    {
        return $this->service->show($id);
    }

    public function destroy($id)
    {
        $this->service->delete($id);
    }

    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }
}
