<?php

namespace codeproject\Http\Controllers;

use codeproject\Repositories\ClientRepository;
use codeproject\Services\ClientService;
use Illuminate\Http\Request;

use codeproject\Http\Requests;

class ClientController extends Controller
{
    private $repository;
    private $clientService;

    public function __construct(ClientRepository $repository, ClientService $clientService)
    {
        $this->clientService = $clientService;
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->repository->all();
    }


    public function store(Request $request)
    {
        return $this->clientService->create($request->all());
    }

    public function show($id)
    {
        return $this->clientService->show($id);
    }

    public function destroy($id)
    {
        $this->clientService->delete($id);
    }

    public function update(Request $request, $id)
    {
        return $this->clientService->update($request->all(), $id);
    }
}
