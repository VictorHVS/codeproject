<?php

use Illuminate\Database\Seeder;

class UserTabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\codeproject\Entities\User::class, 10)->create();
    }
}
