<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTabSeeder::class);
        $this->call(ClientTabSeeder::class);
        $this->call(ProjectTabSeeder::class);
        $this->call(ProjectTaskTabSeeder::class);
        $this->call(ProjectMembersTabSeeder::class);

        Model::reguard();
    }
}
