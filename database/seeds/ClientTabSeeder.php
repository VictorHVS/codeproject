<?php

use Illuminate\Database\Seeder;

class ClientTabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\codeproject\Entities\Client::class, 10)->create();
    }
}
