<?php

use Illuminate\Database\Seeder;

class ProjectTaskTabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\codeproject\Entities\ProjectTask::class, 10)->create();
    }
}
