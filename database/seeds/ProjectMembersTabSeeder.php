<?php

use Illuminate\Database\Seeder;

class ProjectMembersTabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\codeproject\Entities\ProjectMembers::class, 10)->create();
    }
}
